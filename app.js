import express, { Router } from 'express';
import { open } from 'sqlite';
import sqlite3 from 'sqlite3';
import WebSocket from 'ws';
import wss from './wss.js';

async function openDb() {
	return open({ filename: 'database.db', driver: sqlite3.Database });
}

const app = express();
export { app as default };

const router = Router();

app.use(express.json());
app.use('/', router);
app.use(express.static('dist'));

const db = openDb();

// Emptying and re-initialising the database.
/*
async function createDb() {
	(await db).exec('DROP TABLE messages');
	(await db).exec('CREATE TABLE messages (message TEXT, time TEXT, username TEXT)');
}
createDb();
*/

async function postMessage(username, message) {
	const time = new Date().toLocaleString();
	(await db).all(`INSERT INTO messages (message, time, username) VALUES ("${message}", "${time}", "${username}")`);
	wss.clients.forEach(client => {
		if (client.readyState === WebSocket.OPEN) {
			client.send(JSON.stringify({ message, time, username }));
		}
	});
}

router.post(
	'/messages',
	(req, res) => {
		if ('username' in req.body && 'message' in req.body) {
			postMessage(req.body.username, req.body.message).then(
				() => {
					res.end();
				},
				err => {
					console.log(err);
					res.status(500).send(err);
				}
			);
		} else {
			const err = 'The request has to provide a username and a message.';
			console.log(err);
			res.status(400).send(err);
		}
	}
);
