import React from 'react';
import Message from './Message.js';
import Messages from './Messages.js';

export default function App() {
	return (
		<>
			<Messages />
			<Message />
		</>
	);
}
