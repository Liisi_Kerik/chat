import React, { useEffect, useLayoutEffect, useState } from 'react';

function useWebSocket(openCallback, closeCallback, messageCallback) {
	const [websocket, setWebsocket] = useState(null);

	useEffect(() => {
		if (!websocket) {
			setWebsocket(new WebSocket(`${window.location.protocol.replace(/^http/, 'ws')}//${window.location.host}/messages`));
		}
	}, [websocket]);

	useLayoutEffect(() => {
		if (websocket) {
			websocket.onopen = () => {
				console.log('opened');
				openCallback();
			};

			websocket.onclose = () => {
				console.log('closed');
				closeCallback();
			};

			websocket.onmessage = message => {
				console.log(message);
				messageCallback(JSON.parse(message.data));
			};
		}
	}, [websocket, openCallback, closeCallback, messageCallback]);
}

function useMessages() {
	const [messages, setMessages] = useState(null);

	useWebSocket(
		() => { setMessages([]); },
		() => { setMessages(null); },
		message => { setMessages([...messages, message]); }
	);

	return messages;
}

export default function Messages() {
	const messages = useMessages();

	if (messages === null) {
		return <>Loading</>;
	}

	// return (<div dangerouslySetInnerHTML={{ __html: messages.map(message =>
	// `<p>${message.time} <b>${message.username}:</b> ${message.message}</p>`).join('') }} />);
	return (
		<div>
			{messages.map((message, i) => (
				<p key={i}>
					{message.time} <b>{message.username}:</b> {message.message}
				</p>
			))}
		</div>
	);
}
