import React, { useState } from 'react';

function keyPress(event, username, message, setMessage) {
	if (event.charCode === 13) {
		fetch(
			'/messages',
			{
				body: `{"message": "${message}", "username": "${username}"}`,
				headers: { 'Content-Type': 'application/json' },
				method: 'POST'
			}
		).then(response => { response.blob(); console.log(response); });
		setMessage('');
	}
}

export default function Message() {
	const [username, setUsername] = useState('');
	const [message, setMessage] = useState('');

	return (
		<table>
			<tbody>
				<tr>
					<td>
						Name:
					</td>
					<td>
						<input id="Username" value={username} onInput={event => setUsername(event.target.value)} />
					</td>
				</tr>
				<tr>
					<td>
						Message:
					</td>
					<td>
						<input id="Message" onInput={event => setMessage(event.target.value)} onKeyPress={event => keyPress(event, username, message, setMessage)} value={message} />
					</td>
				</tr>
			</tbody>
		</table>
	);
}
