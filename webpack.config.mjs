import path from 'path';
import url from 'url';
import HtmlWebpackPlugin from 'html-webpack-plugin';

const directoryName = path.dirname(url.fileURLToPath(import.meta.url));

const backendPort = process.env.port ?? 3000;

export default {
	entry: path.join(directoryName, 'client', 'index.js'),

	// Kui seda ei ole, siis võivad mingid URL'id asset'itele katki olla.
	// Täpselt ei mäleta enam, mis katki läheb :D
	output: {
		publicPath: '/'
	},

	plugins: [
		// See genereerib lihtsa `index.html` faili ja sinna `script` tag'i genereeritud skriptifailiga.
		new HtmlWebpackPlugin()
	],

	module: {
		rules: [
			// React'i failide jaoks
			{
				test: /\.js$/,
				exclude: path.join(directoryName, 'node_modules'),
				use: 'babel-loader'
			},

			// Et saaks CSS faile importida otse skriptifailidest
			{
				test: /\.css$/i,
				include: path.join(directoryName, 'node_modules'),
				use: ['style-loader', 'css-loader']
			},

			// Et saaks pilte importida otse skriptidest - pole vist lihtsa use-case'i jaoks vajalik
			{
				test: /\.(png|jpg|gif)$/,
				type: 'asset/resource'
			}
		]
	},
	devServer: {
		// `dist` on vaikimisi koht, kuhu WebPack assetid installitb.
		// (ma pean igaks juhuks üle kontorillima,
		// kas see konf on üldse vajalik,
		// sest WebPack võiks ise teada, kust faile võtta :))
		static: path.join(directoryName, 'dist'),

		// Paneb webpack-dev-server'i backend'i emuleerima.
		// On ka teistsuguseid setupi võimalusi
		// (nt. backend on täiesti eraldi ja front-end ühendub otse selle külge).
		// Proxy setup'i eelis on lihtsus - rakendus on ühtne tervik, mitte backend pole eraldi
		proxy: {
			'/messages': {
				target: `ws://localhost:${backendPort}`,
				ws: true
			}
		}
	}
};
