import ws from 'ws';

const wss = new ws.Server({ noServer: true });

export { wss as default };

wss.on('connection', ws => {
	ws.on('message', message => {
		console.log(`received: ${message}`);
	});
});
