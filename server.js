import http from 'http';
import app from './app.js';
import wss from './wss.js';

const server = http.createServer(app);

server.listen(
	process.env.port || 3000,
	() => {
		const address = server.address();
		console.log(`Listening on ${address.address}:${address.port}`);
	}
);

server.on('upgrade', (request, socket, head) => {
	wss.handleUpgrade(request, socket, head, ws => {
		wss.emit('connection', ws, request);
	});
});
